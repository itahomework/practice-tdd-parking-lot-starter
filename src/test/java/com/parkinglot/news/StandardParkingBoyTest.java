package com.parkinglot.news;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingTicket;
import com.parkinglot.boy.StandardParkingBoy;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StandardParkingBoyTest {

    @Test
    void should_return_parking_ticket_when_park_given_parking_boy_and_parking_car_and_parking_lot(){
        // given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        ParkingLot parkingLot = new ParkingLot(10);
        standardParkingBoy.setParkingLotList(parkingLot);
        Car car = new Car("粤A0000");
        // when
        ParkingTicket ticket = standardParkingBoy.park(car);
        // then
        assertEquals(ticket.getLicensePlate(), car.getLicensePlate());
        assertEquals(ticket.getClass(), ParkingTicket.class);
    }

    @Test
    void should_return_car_when_fetch_given_parking_boy_and_parking_ticket_and_parking_lot(){
        // given
        ParkingLot parkingLot = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.setParkingLotList(parkingLot);
        Car car = new Car("粤A0000");
        ParkingTicket ticket = standardParkingBoy.park(car);
        // when
        Car fetchCar = standardParkingBoy.fetch(ticket);
        // then
        assertEquals(fetchCar, car);
    }

    @Test
    void should_throw_runtime_exception_when_fetch_given_wrong_parking_ticket_and_parking_lot_and_parking_boy(){
        // given
        ParkingLot parkingLot = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.setParkingLotList(parkingLot);
        Car car = new Car("粤A0000");
        ParkingTicket ticket = standardParkingBoy.park(car);
        ParkingTicket wrongTicket = new ParkingTicket();
        // when
        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            Car fetchCar = standardParkingBoy.fetch(wrongTicket);
        });
        // then
        assertEquals(exception.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throw_runtime_exception_when_fetch_given_used_parking_ticket_and_parking_lot_and_parking_boy(){
        // given
        ParkingLot parkingLot = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.setParkingLotList(parkingLot);
        Car car = new Car("粤A0000");
        ParkingTicket ticket = standardParkingBoy.park(car);
        standardParkingBoy.fetch(ticket);
        // when
        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            Car fetchCar = standardParkingBoy.fetch(ticket);
        });
        // then
        assertEquals(exception.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_return_equal_two_car_false_when_fetch_given_parking_ticket_and_car_and_parking_boy(){
        // given
        ParkingLot parkingLot = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.setParkingLotList(parkingLot);
        Car car = new Car("粤A0000");
        ParkingTicket ticket = standardParkingBoy.park(car);
        // when
        Car fetchCar = standardParkingBoy.fetch(ticket);
        // then
        assertEquals(fetchCar.getLicensePlate(), ticket.getLicensePlate());
    }

    @Test
    void should_throw_runtime_exception_when_park_given_full_parking_lot_and_car_and_parking_boy(){
        // given
        ParkingLot parkingLot = new ParkingLot(1);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.setParkingLotList(parkingLot);
        Car car1 = new Car("粤A0000");
        Car car2 = new Car("粤B0000");
        ParkingTicket parkingTicket1 = standardParkingBoy.park(car1);
        // when
        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            ParkingTicket parkingTicket2 = standardParkingBoy.park(car2);
        });
        // then
        assertEquals(exception.getMessage(), "No available position");
    }

    // parkingLotList
    @Test
    void should_return_parking_ticket_when_park_given_parking_boy_and_parking_car_and_2_parking_lots(){
        // given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        standardParkingBoy.setParkingLotList(parkingLot1, parkingLot2);
        Car car = new Car("粤A0000");
        // when
        ParkingTicket ticket = standardParkingBoy.park(car);
        // then
        assertEquals(ticket.getLicensePlate(), car.getLicensePlate());
        assertEquals(ticket.getClass(), ParkingTicket.class);
    }

    @Test
    void should_return_car_in_lot2_is_true_when_park_given_parking_boy_and_parking_car_and_2_parking_lots(){
        // given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(10);
        standardParkingBoy.setParkingLotList(parkingLot1, parkingLot2);
        Car car = new Car("粤A0000");
        // when
        ParkingTicket ticket = standardParkingBoy.park(car);
        // then
        assertEquals(parkingLot2.getTicketMappingCarMap().get(ticket), car);
    }

    @Test
    void should_return_car_when_fetch_given_parking_ticket_and_2_parking_lots(){
        // given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        standardParkingBoy.setParkingLotList(parkingLot1, parkingLot2);
        Car car = new Car("粤A0000");
        ParkingTicket ticket = standardParkingBoy.park(car);
        // when
        Car fetchedCar = standardParkingBoy.fetch(ticket);
        // then
        assertEquals(car, fetchedCar);
    }

    @Test
    void should_throw_runtime_exception_when_fetch_given_wrong_parking_ticket_and_2_parking_lots(){
        // given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        standardParkingBoy.setParkingLotList(parkingLot1, parkingLot2);
        ParkingTicket wrongTicket = new ParkingTicket("1");
        // when
        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            Car fetchCar = standardParkingBoy.fetch(wrongTicket);
        });
        // then
        assertEquals(exception.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throw_runtime_exception_when_fetch_given_used_parking_ticket_and_2_parking_lots_and_parking_boy(){
        // given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.setParkingLotList(parkingLot1, parkingLot2);
        Car car = new Car("粤A0000");
        ParkingTicket ticket = standardParkingBoy.park(car);
        standardParkingBoy.fetch(ticket);
        // when
        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            Car fetchCar = standardParkingBoy.fetch(ticket);
        });
        // then
        assertEquals(exception.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throw_runtime_exception_when_park_given_full_parking_lot_and_not_full_parking_lot_and_car_and_parking_boy(){
        // given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.setParkingLotList(parkingLot1, parkingLot2);
        Car car1 = new Car("粤A0000");
        Car car2 = new Car("粤B0000");
        Car car3 = new Car("粤C0000");
        standardParkingBoy.park(car1);
        standardParkingBoy.park(car2);
        // when
        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            ParkingTicket parkingTicket2 = standardParkingBoy.park(car3);
        });
        // then
        assertEquals(exception.getMessage(), "No available position");
    }

}
