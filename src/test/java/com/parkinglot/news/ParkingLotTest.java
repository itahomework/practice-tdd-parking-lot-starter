package com.parkinglot.news;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingTicket;
import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecognizedParkingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {

    @Test
    void should_return_parking_ticket_when_park_given_parking_lot_and_car(){
        // given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        // when
        ParkingTicket parkingTicket = parkingLot.park(car);
        // then
        assertEquals(parkingTicket.getClass(), ParkingTicket.class);
    }

    @Test
    void should_return_equal_two_parkingTicket_false_when_park_given_parking_lot_and_two_car(){
        // given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car1 = new Car("粤A0000");
        Car car2 = new Car("粤B0000");
        // when
        ParkingTicket parkingTicket1 = parkingLot.park(car1);
        ParkingTicket parkingTicket2 = parkingLot.park(car2);
        // then
        assertNotEquals(parkingTicket2, parkingTicket1);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_car(){
        // given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car("粤A0000");
        ParkingTicket ticket = parkingLot.park(car);
        // when
        Car fetchCar = parkingLot.fetch(ticket);
        // then
        assertEquals(fetchCar, car);
    }

    @Test
    void should_throw_runtime_exception_when_fetch_given_wrong_parking_ticket_and_parking_lot(){
        // given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car("粤A0000");
        ParkingTicket ticket = parkingLot.park(car);
        ParkingTicket wrongTicket = new ParkingTicket();
        // when
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> {
            Car fetchCar = parkingLot.fetch(wrongTicket);
        });
        // then
        assertEquals(exception.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throw_runtime_exception_when_fetch_given_used_parking_ticket_and_parking_lot(){
        // given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car("粤A0000");
        ParkingTicket ticket = parkingLot.park(car);
        parkingLot.fetch(ticket);
        // when
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> {
            Car fetchCar = parkingLot.fetch(ticket);
        });
        // then
        assertEquals(exception.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_return_equal_two_car_false_when_fetch_given_parking_ticket_and_car(){
        // given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car("粤A0000");
        ParkingTicket ticket = parkingLot.park(car);
        // when
        Car fetchCar = parkingLot.fetch(ticket);
        // then
        assertEquals(fetchCar.getLicensePlate(), ticket.getLicensePlate());
    }

    @Test
    void should_throw_runtime_exception_when_park_given_full_parking_lot_and_car(){
        // given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car1 = new Car("粤A0000");
        Car car2 = new Car("粤B0000");
        ParkingTicket parkingTicket1 = parkingLot.park(car1);
        // when
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException .class, () -> {
            ParkingTicket parkingTicket2 = parkingLot.park(car2);
        });
        // then
        assertEquals(exception.getMessage(), "No available position");
    }
}
