package com.parkinglot.news;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingLotServiceManager;
import com.parkinglot.ParkingTicket;
import com.parkinglot.boy.ParkingBoy;
import com.parkinglot.boy.SmartParkingBoy;
import com.parkinglot.boy.StandardParkingBoy;
import com.parkinglot.boy.SuperSmartParkingBoy;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ParkingLotServiceManagerTest {

    @Test
    void should_parking_boy_list_contain_parking_boy_when_addParkingBoy_given_manager_and_3_types_parking_boy(){
        // given
        ParkingLotServiceManager manager = new ParkingLotServiceManager();
        ParkingBoy standardParkingBoy = new StandardParkingBoy();
        ParkingBoy smartBoy = new SmartParkingBoy();
        ParkingBoy superSmartBoy = new SuperSmartParkingBoy();
        // when
        manager.addParkingBoy(smartBoy);
        manager.addParkingBoy(standardParkingBoy);
        manager.addParkingBoy(superSmartBoy);
        // then
        assertEquals(manager.getParkingBoyList().get(0), smartBoy);
        assertEquals(manager.getParkingBoyList().get(1), standardParkingBoy);
        assertEquals(manager.getParkingBoyList().get(2), superSmartBoy);
    }

    @Test
    void should_return_car_is_in_lot_2_when_park_given_full_parking_lot_and_not_full_parking_lot_and_car_and_lot_manager(){
        // given
        ParkingLotServiceManager manager = new ParkingLotServiceManager();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(10);
        manager.setParkingLotList(parkingLot1, parkingLot2);
        parkingLot1.park(new Car("粤B1111"));
        Car car = new Car("粤A0000");
        // when
        ParkingTicket ticket = manager.park(car);
        // then
        assertEquals(car, parkingLot2.getTicketMappingCarMap().get(ticket));
    }

    @Test
    void should_return_car_when_fetch_given_parking_ticket_and_2_parking_lots(){
        // given
        ParkingLotServiceManager manager = new ParkingLotServiceManager();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        manager.setParkingLotList(parkingLot1, parkingLot2);
        Car car = new Car("粤A0000");
        ParkingTicket ticket = manager.park(car);
        // when
        Car fetchedCar = manager.fetch(ticket);
        // then
        assertEquals(car, fetchedCar);
    }

    @Test
    void should_throw_runtime_exception_when_fetch_given_wrong_parking_ticket_and_2_parking_lots(){
        // given
        ParkingLotServiceManager manager = new ParkingLotServiceManager();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        manager.setParkingLotList(parkingLot1, parkingLot2);
        ParkingTicket wrongTicket = new ParkingTicket("1");
        // when
        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            Car fetchCar = manager.fetch(wrongTicket);
        });
        // then
        assertEquals(exception.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throw_runtime_exception_when_fetch_given_used_parking_ticket_and_2_parking_lots_and_parking_boy(){
        // given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        ParkingLotServiceManager manager = new ParkingLotServiceManager();
        manager.setParkingLotList(parkingLot1, parkingLot2);
        Car car = new Car("粤A0000");
        ParkingTicket ticket = manager.park(car);
        manager.fetch(ticket);
        // when
        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            Car fetchCar = manager.fetch(ticket);
        });
        // then
        assertEquals(exception.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throw_runtime_exception_when_park_given_full_parking_lot_and_not_full_parking_lot_and_car_and_parking_boy(){
        // given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingLotServiceManager manager = new ParkingLotServiceManager();
        manager.setParkingLotList(parkingLot1, parkingLot2);
        Car car1 = new Car("粤A0000");
        Car car2 = new Car("粤B0000");
        Car car3 = new Car("粤C0000");
        manager.park(car1);
        manager.park(car2);
        // when
        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            ParkingTicket parkingTicket2 = manager.park(car3);
        });
        // then
        assertEquals(exception.getMessage(), "No available position");
    }
}
