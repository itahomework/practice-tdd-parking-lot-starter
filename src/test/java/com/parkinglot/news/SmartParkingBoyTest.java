package com.parkinglot.news;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingTicket;
import com.parkinglot.boy.ParkingBoy;
import com.parkinglot.boy.SmartParkingBoy;
import com.parkinglot.boy.SmartParkingBoy;
import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecognizedParkingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SmartParkingBoyTest {

    @Test
    void should_return_car_is_in_lot_2_when_park_given_smart_parking_boy_and_parking_car_and_2_parking_lots(){
        // given
        ParkingBoy smartParkingBoy = new SmartParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(10);
        smartParkingBoy.setParkingLotList(parkingLot1, parkingLot2);
        Car car = new Car("粤A0000");
        // when
        ParkingTicket ticket = smartParkingBoy.park(car);
        // then
        assertEquals(car, parkingLot2.getTicketMappingCarMap().get(ticket));
    }

    @Test
    void should_return_parking_ticket_when_park_given_parking_boy_and_parking_car_and_2_parking_lots(){
        // given
        ParkingBoy smartParkingBoy = new SmartParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        smartParkingBoy.setParkingLotList(parkingLot1, parkingLot2);
        Car car = new Car("粤A0000");
        // when
        ParkingTicket ticket = smartParkingBoy.park(car);
        // then
        assertEquals(ticket.getLicensePlate(), car.getLicensePlate());
        assertEquals(ticket.getClass(), ParkingTicket.class);
    }

    @Test
    void should_return_car_when_fetch_given_parking_ticket_and_2_parking_lots(){
        // given
        ParkingBoy smartParkingBoy = new SmartParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        smartParkingBoy.setParkingLotList(parkingLot1, parkingLot2);
        Car car = new Car("粤A0000");
        ParkingTicket ticket = smartParkingBoy.park(car);
        // when
        Car fetchedCar = smartParkingBoy.fetch(ticket);
        // then
        assertEquals(car, fetchedCar);
    }

    @Test
    void should_throw_runtime_exception_when_fetch_given_wrong_parking_ticket_and_2_parking_lots(){
        // given
        ParkingBoy smartParkingBoy = new SmartParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        smartParkingBoy.setParkingLotList(parkingLot1, parkingLot2);
        ParkingTicket wrongTicket = new ParkingTicket("1");
        // when
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> {
            Car fetchCar = smartParkingBoy.fetch(wrongTicket);
        });
        // then
        assertEquals(exception.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throw_runtime_exception_when_fetch_given_used_parking_ticket_and_2_parking_lots_and_parking_boy(){
        // given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        ParkingBoy smartParkingBoy = new SmartParkingBoy();
        smartParkingBoy.setParkingLotList(parkingLot1, parkingLot2);
        Car car = new Car("粤A0000");
        ParkingTicket ticket = smartParkingBoy.park(car);
        smartParkingBoy.fetch(ticket);
        // when
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> {
            Car fetchCar = smartParkingBoy.fetch(ticket);
        });
        // then
        assertEquals(exception.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throw_runtime_exception_when_park_given_full_parking_lot_and_not_full_parking_lot_and_car_and_parking_boy(){
        // given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingBoy smartParkingBoy = new SmartParkingBoy();
        smartParkingBoy.setParkingLotList(parkingLot1, parkingLot2);
        Car car1 = new Car("粤A0000");
        Car car2 = new Car("粤B0000");
        Car car3 = new Car("粤C0000");
        smartParkingBoy.park(car1);
        smartParkingBoy.park(car2);
        // when
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> {
            ParkingTicket parkingTicket2 = smartParkingBoy.park(car3);
        });
        // then
        assertEquals(exception.getMessage(), "No available position");
    }
}
