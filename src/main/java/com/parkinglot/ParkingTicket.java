package com.parkinglot;

import java.util.Objects;

public class ParkingTicket {
    private String licensePlate;

    public ParkingTicket(){}

    public ParkingTicket(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParkingTicket that = (ParkingTicket) o;
        return Objects.equals(licensePlate, that.licensePlate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(licensePlate);
    }

    public String getLicensePlate() {
        return licensePlate;
    }
}
