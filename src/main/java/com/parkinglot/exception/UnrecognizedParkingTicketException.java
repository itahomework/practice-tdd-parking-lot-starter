package com.parkinglot.exception;

public class UnrecognizedParkingTicketException extends RuntimeException{
    public UnrecognizedParkingTicketException(){
        super("Unrecognized parking ticket");
    }

    public UnrecognizedParkingTicketException(String s){
        super(s);
    }
}
