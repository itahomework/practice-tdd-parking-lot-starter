package com.parkinglot.behavior;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingTicket;

import java.util.List;

public interface ParkBehavior {
    ParkingTicket park(Car car, List<ParkingLot> parkingLotList);
}
