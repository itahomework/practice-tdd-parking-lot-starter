package com.parkinglot.behavior;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingTicket;
import com.parkinglot.exception.NoAvailablePositionException;

import java.util.Comparator;
import java.util.List;

public class SmartParkBehavior implements ParkBehavior{
    @Override
    public ParkingTicket park(Car car, List<ParkingLot> parkingLotList) {
        ParkingLot maxParkingLot = parkingLotList.stream()
                .max(Comparator.comparingInt(ParkingLot::getRemainParkingSpaces))
                .orElse(null);
        if (maxParkingLot == null) {
            throw new NoAvailablePositionException();
        }
        return maxParkingLot.park(car);
    }
}
