package com.parkinglot.behavior;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingTicket;
import com.parkinglot.exception.NoAvailablePositionException;

import java.util.List;

public class StandardParkBehavior implements ParkBehavior{
    @Override
    public ParkingTicket park(Car car, List<ParkingLot> parkingLotList) {
        return parkingLotList.
                stream().
                filter(parkingLot -> !parkingLot.isFull()).
                findFirst().
                orElseThrow(NoAvailablePositionException::new)
                .park(car);
    }
}
