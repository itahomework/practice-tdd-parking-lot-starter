package com.parkinglot;

import com.parkinglot.boy.SmartParkingBoy;
import com.parkinglot.boy.StandardParkingBoy;
import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecognizedParkingTicketException;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {

    private Map<ParkingTicket, Car> ticketMappingCarMap;
    private int capacity;

    public Map<ParkingTicket, Car> getTicketMappingCarMap() {
        return ticketMappingCarMap;
    }

    public ParkingLot(int capacity){
        ticketMappingCarMap = new HashMap<>();
        this.capacity = capacity;
    }
    public ParkingTicket park(Car car) {
        if (isFull()){
//            throw new RuntimeException("No available position");
            throw new NoAvailablePositionException();
        }
        ParkingTicket ticket = new ParkingTicket(car.getLicensePlate());
        ticketMappingCarMap.put(ticket, car);
        return ticket;
    }

    public Car fetch(ParkingTicket ticket) {
        if (!isValidTicket(ticket)){
//            throw new RuntimeException("Unrecognized parking ticket");
            throw new UnrecognizedParkingTicketException();
        }
        Car car = ticketMappingCarMap.get(ticket);
        ticketMappingCarMap.remove(ticket);
        return car;
    }

    public boolean isFull(){
        return ticketMappingCarMap.size() >= capacity;
    }

    public boolean isValidTicket(ParkingTicket ticket){
        return ticketMappingCarMap.containsKey(ticket);
    }

    public int getRemainParkingSpaces(){
        return this.capacity - this.ticketMappingCarMap.size();
    }

    public double getAvailablePositionRate(){
        return (double) getRemainParkingSpaces() / this.capacity;
    }
}
