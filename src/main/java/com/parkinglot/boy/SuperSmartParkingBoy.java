package com.parkinglot.boy;

import com.parkinglot.ParkingLot;
import com.parkinglot.behavior.SuperSmartParkBehavior;

public class SuperSmartParkingBoy extends ParkingBoy{

    public SuperSmartParkingBoy(ParkingLot... parkingLot) {
        super(parkingLot);
        this.parkBehavior = new SuperSmartParkBehavior();
    }
}
