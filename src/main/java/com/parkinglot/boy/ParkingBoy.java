package com.parkinglot.boy;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingTicket;
import com.parkinglot.behavior.ParkBehavior;
import com.parkinglot.exception.UnrecognizedParkingTicketException;

import java.util.List;

public abstract class ParkingBoy {
    protected List<ParkingLot> parkingLotList;
    protected ParkBehavior parkBehavior;
    public ParkingBoy(ParkingLot... parkingLot) {
        this.parkingLotList = List.of(parkingLot);
    }
    public void setParkingLotList(ParkingLot... parkingLot) {
        this.parkingLotList = List.of(parkingLot);
    }


    public ParkingTicket park(Car car){
        return parkBehavior.park(car, parkingLotList);
    }

    public Car fetch(ParkingTicket ticket) {
        return parkingLotList.
                stream().
                filter(parkingLot -> parkingLot.isValidTicket(ticket)).
                findFirst().
                orElseThrow(UnrecognizedParkingTicketException::new)
                .fetch(ticket);
    }

}
