package com.parkinglot.boy;

import com.parkinglot.ParkingLot;
import com.parkinglot.behavior.StandardParkBehavior;


public class StandardParkingBoy extends ParkingBoy{

    public StandardParkingBoy(ParkingLot... parkingLot) {
        super(parkingLot);
        this.parkBehavior = new StandardParkBehavior();
    }

}
