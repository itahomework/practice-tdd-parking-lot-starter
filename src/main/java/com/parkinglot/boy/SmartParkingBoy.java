package com.parkinglot.boy;

import com.parkinglot.ParkingLot;
import com.parkinglot.behavior.SmartParkBehavior;


public class SmartParkingBoy extends ParkingBoy {
    public SmartParkingBoy(ParkingLot... parkingLot) {
        super(parkingLot);
        this.parkBehavior = new SmartParkBehavior();
    }

}
