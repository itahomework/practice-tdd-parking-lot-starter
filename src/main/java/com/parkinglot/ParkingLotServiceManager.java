package com.parkinglot;

import com.parkinglot.behavior.*;
import com.parkinglot.boy.ParkingBoy;
import com.parkinglot.exception.UnrecognizedParkingTicketException;

import java.util.ArrayList;
import java.util.List;

public class ParkingLotServiceManager {
    private List<ParkingBoy> parkingBoyList;
    protected ParkBehavior parkBehavior;
    protected List<ParkingLot> parkingLotList;

    public void setParkingLotList(ParkingLot... parkingLot) {
        this.parkingLotList = List.of(parkingLot);
    }

    public ParkingLotServiceManager() {
       this.parkingBoyList = new ArrayList<>();
       this.parkingLotList = new ArrayList<>();
       this.parkBehavior = new StandardParkBehavior();
    }

    public void setParkingBoyList(List<ParkingBoy> parkingBoyList) {
        this.parkingBoyList = parkingBoyList;
    }

    public List<ParkingBoy> getParkingBoyList() {
        return parkingBoyList;
    }

    public void addParkingBoy(ParkingBoy boy){
        this.parkingBoyList.add(boy);
    }

//    public ParkingTicket specifyParkingBoyPark(ParkingBoy parkingBoy, Car car){
//        try {
//            return parkingBoy.park(car);
//        }catch (NoAvailablePositionException e){
//            throw new NoAvailablePositionException();
//        }catch (UnrecognizedParkingTicketException e){
//            throw new UnrecognizedParkingTicketException();
//        }
//    }

    public ParkingTicket park(Car car){
        ParkingTicket ticket;
        for (ParkingBoy boy : parkingBoyList){
            try {
                ticket = boy.park(car);
                return ticket;
            }catch (Exception e){

            }
        }
//        parkBehavior.park(car, parkingLotList);
        return parkBehavior.park(car, parkingLotList);
    }

    public Car fetch(ParkingTicket parkingTicket){
        Car car;
        for (ParkingBoy boy : parkingBoyList){
            try {
                car = boy.fetch(parkingTicket);
                return car;
            }catch (Exception e){

            }
        }
        return parkingLotList.
                stream().
                filter(parkingLot -> parkingLot.isValidTicket(parkingTicket)).
                findFirst().
                orElseThrow(UnrecognizedParkingTicketException::new)
                .fetch(parkingTicket);
    }
}
