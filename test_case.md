### story1

1. **Given** a parking lot, and a car,  **When** park the car,  **Then** return a parking ticket.
2. **Given** a parking lot, and two car,  **When** park two car,  **Then** return  two ticket is not equal.
3. **Given** a parking ticket, and a parking lot,  **When** fetch the car,  **Then** return a car .
4. **Given** a fail parking ticket, and a parking lot,  **When** fetch the car,  **Then** return null.
5. **Given** a used parking ticket, and a parking lot,  **When** fetch the car,  **Then** return null.
6. **Given** a parking ticket, and a parking lot,  **When** fetch the car,  **Then** return whether fetchCar is true.
7. **Given** a full parking lot, and a car,  **When** park the car,  **Then** return null.



### story2

1. **Given** a parking lot, and a car,  **When** park the car,  **Then** return a parking ticket.
2. **Given** a parking lot, and two car,  **When** park two car,  **Then** return  two ticket is not equal.
3. **Given** a parking ticket, and a parking lot,  **When** fetch the car,  **Then** return a car .
4. **Given** a fail parking ticket, and a parking lot,  **When** fetch the car,  **Then** throw UnrecognizedParkingTicketException.
5. **Given** a used parking ticket, and a parking lot,  **When** fetch the car,  **Then** throw UnrecognizedParkingTicketException.
6. **Given** a parking ticket, and a parking lot,  **When** fetch the car,  **Then** return whether fetchCar is true.
7. **Given** a full parking lot, and a car,  **When** park the car,  **Then** throw NoAvailablePositionException.



### story 3

1. **Given** a parking boy, and a parking lot,  **When** park the car,  **Then** return a parking ticket.
2. **Given** a parking boy, a parking lot, and a parking ticket,  **When** fetch the car,  **Then** return a car.
3. **Given** a fail parking ticket, a parking boy and a parking lot,  **When** fetch the car,  **Then** throw exception.
4. **Given** a used parking ticket, a parking boy, and a parking lot,  **When** fetch the car,  **Then** throw exception.
5. **Given** a parking ticket, a parking boy, and a parking lot,  **When** fetch the car,  **Then** return whether fetchCar is true.
6. **Given** a full parking lot, a parking boy, and a car,  **When** park the car,  **Then** throw exception.



### story 4 

1. **Given** a parking boy,  parking lot A and parking lot B,  **When** park the car,  **Then** parking lot A has car and return ticket.
2. **Given** a parking boy, full parking lot A and parking lot B,  **When** park the car,  **Then** parking lot B has car and return ticket.
3. **Given** a fail parking ticket, and 2 parking lot,  **When** fetch the car,  **Then** throw exception.
4. **Given** a used parking ticket, and 2 parking lot,  **When** fetch the car,  **Then** throw exception.
5. **Given** a parking ticket, and 2 parking lot,  **When** fetch the car,  **Then** return whether fetchCar is true.
6. **Given**  full parking lot A and B, and a car,  **When** park the car,  **Then** throw exception.



### story5

1. **Given** a smart parking boy,  parking lot A and parking lot B,  **When** park the car,  **Then** parking lot A has car and return ticket.
2. **Given** a smart parking boy, full parking lot A and parking lot B,  **When** park the car,  **Then** parking lot B has car and return ticket.
3. **Given** a smart parking boy,a fail parking ticket, and 2 parking lot,  **When** fetch the car,  **Then** throw exception.
4. **Given** a smart parking boy, a used parking ticket, and 2 parking lot,  **When** fetch the car,  **Then** throw exception.
5. **Given** a smart parking boy, a parking ticket, and 2 parking lot,  **When** fetch the car,  **Then** return whether fetchCar is true.
6. **Given** a smart parking boy, full parking lot A and B, and a car,  **When** park the car,  **Then** throw exception.

### story6

1. **Given** a super smart parking boy,  parking lot A and parking lot B,  **When** park the car,  **Then** parking lot A has car and return ticket.
2. **Given** a super smart parking boy, full parking lot A and parking lot B,  **When** park the car,  **Then** parking lot B has car and return ticket.
3. **Given** a super smart parking boy,a fail parking ticket, and 2 parking lot,  **When** fetch the car,  **Then** throw exception.
4. **Given** a super smart parking boy, a used parking ticket, and 2 parking lot,  **When** fetch the car,  **Then** throw exception.
5. **Given** a super smart parking boy, a parking ticket, and 2 parking lot,  **When** fetch the car,  **Then** return whether fetchCar is true.
6. **Given** a super smart parking boy, full parking lot A and B, and a car,  **When** park the car,  **Then** throw exception.

### story7

1. **Given** a parkingManager,  parking full lot A and parking lot B,  **When**  park the car,  **Then** parking lot B has car and return ticket.
2. **Given** a parking ticket, parkingManager, and 2 parking lot,  **When** fetch the car,  **Then** return whether fetchCar is true.
3. **Given** a fail parking ticket, and 2 parking lot,  and parkingManager **When** fetch the car,  **Then** throw exception.
4. **Given** a used parking ticket, and 2 parking lot, and parkingManager **When** fetch the car,  **Then** throw exception.
5. **Given**  full parking lot A and B, a car, and parkingManager **When** park the car,  **Then** throw exception.
6. **Given** a parkingBoyList,  parking lot A, parking lot B,and parkingManager  **When** use standardParkingBoy park the car,  **Then** parking lot B has car and return ticket.
7. **Given** a parkingBoyList,  parking lot A, parking lot B,and parkingManager  **When** use smartParkingBoy park the car,  **Then** parking lot B has car and return ticket.
8. **Given** a parkingBoyList,  parking lot A, parking lot B,and parkingManager  **When** use superSmartParkingBoy park the car,  **Then** parking lot B has car and return ticket.